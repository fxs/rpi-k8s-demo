
ANSIBLE-LINT = ansible-lint
SSH          = ssh
GIT          = git
DOCKER       = docker
KUBECTL      = kubectl

IMAGE_ANSIBLE = jfxs/ansible:2.9
IMAGE_INSPEC  = chef/inspec

INVENTORY_FILE = .ansible/inventory
INVENTORY_FILE_VAULT = files/inventory.vault
ROOT_PLAYBOOK = provisioning/site.yml

KEY_PATH = files/keys

## Ansible management
## ------------------
provision: ## Provisioning with Ansible. Argument: group=bastions args=provisioning/core.yml
provision:
	test -n "${group}"  # Failed if group not set
	test -n "${args}"  # Failed if args not set
	$(eval start := $(shell date))
	@if [ -z ${container} ]; then \
		$(DOCKER) run -t --rm -v ${PWD}:/ansible -e ANSIBLE_CONFIG=.ansible/ansible.cfg ${IMAGE_ANSIBLE} \
		ansible-playbook --limit ${group} -i ${INVENTORY_FILE} ${args}; \
	else \
		export ANSIBLE_CONFIG=.ansible/ansible.cfg && ansible-playbook --limit ${group} -i ${INVENTORY_FILE} ${args}; \
	fi
	@echo "---"
	@echo "Start: ${start} - End: $$(date)"

host-init: ## Init new server. First connexion to change password necessary Argument:  host=new_hostname host_ip=ip_dhcp password=ubuntu_password
host-init:
	test -n "${host}"  # Failed if host_name not set
	test -n "${host_ip}"  # Failed if host not set
	test -n "${password}"  # Failed if password not set
	$(eval new_host_ip := $(shell cat ${INVENTORY_FILE} | grep ${host} | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}' | sort -u | head -n 1))
	$(eval host_key := $(shell cat ${INVENTORY_FILE} | grep ${host} | grep  ansible_ssh_private_key_file | sort -u | head -n 1 | sed -e "s/.*ansible_ssh_private_key_file=//" -e "s/\.pem//"))
	@echo "New IP for host: ${new_host_ip}"
	@if [ -z ${container} ]; then \
		$(DOCKER) run -t --rm -v ${PWD}:/ansible -e ANSIBLE_CONFIG=.ansible/ansible.cfg ${IMAGE_ANSIBLE} \
		ansible-playbook -i ${host_ip}, -e "ansible_user=ubuntu ansible_password=${password} key_name=${host_key} host_ip=${new_host_ip} host_name=${host}" provisioning/init.yml; \
	else \
		export ANSIBLE_CONFIG=.ansible/ansible.cfg && ansible-playbook -i ${host_ip}, -e "ansible_user=ubuntu ansible_password=${password} key_name=${host_key} host_ip=${new_host_ip} host_name=${host}" provisioning/init.yml; \
	fi

ansible-vault-encrypt: ## Encrypt Ansible Vault file. Argument: file=files/key.pem
ansible-vault-encrypt:
	test -n "${file}"  # Failed if file path not set
	@$(DOCKER) run -t --rm -v ${PWD}:/ansible -e ANSIBLE_CONFIG=.ansible/ansible.cfg ${IMAGE_ANSIBLE} ansible-vault encrypt ${file}

ansible-vault-edit: ## Edit Ansible Vault file. Argument: file=provisioning/group_vars/all/vault
ansible-vault-edit:
	test -n "${file}"  # Failed if file path not set
	@$(DOCKER) run -it --rm -v ${PWD}:/ansible -e ANSIBLE_CONFIG=.ansible/ansible.cfg ${IMAGE_ANSIBLE} ansible-vault edit ${file}

ansible-vault-decrypt: ## Decrypt Ansible Vault file. Argument source=files/key.pem.vault dest=.ssh/key.pem
ansible-vault-decrypt:
	test -n "${source}"  # Failed if file path not set
	test -n "${dest}"  # Failed if file path not set
	@cp -f "${source}" "${dest}"
	@if [ -z ${container} ]; then \
		$(DOCKER) run -t --rm -v ${PWD}:/ansible -e ANSIBLE_CONFIG=.ansible/ansible.cfg ${IMAGE_ANSIBLE} \
		ansible-vault decrypt "${dest}"; \
	else \
		export ANSIBLE_CONFIG=.ansible/ansible.cfg && ansible-vault decrypt "${dest}"; \
	fi
	@chmod 600 "${dest}"

inventory-encrypt: ## Encrypt inventory with Vault.
inventory-encrypt:
	@cp ${INVENTORY_FILE} ${INVENTORY_FILE}.v
	@$(MAKE) ansible-vault-encrypt file=${INVENTORY_FILE}.v
	@mv ${INVENTORY_FILE}.v ${INVENTORY_FILE_VAULT}
	@echo "Inventory file is encrypted"

ansible-galaxy-install: ## Install requirements from Galaxy.
ansible-galaxy-install:
	export ANSIBLE_CONFIG=.ansible/ansible.cfg && ansible-galaxy install --force -r provisioning/requirements.yml

.PHONY: provision host-init ansible-vault-encrypt ansible-vault-edit ansible-vault-decrypt ansible-galaxy-install

## Test
## ----
ansible-lint: ## Check syntax
ansible-lint:
	@$(ANSIBLE-LINT) --exclude .ansible/roles ${ROOT_PLAYBOOK}

inspec-dev-sec-bastion: ## dev-sec.io tests with Inspec for bastion. Argument: [reporter=progress|json|...]
inspec-dev-sec-bastion:
	$(eval bastion_ip := $(shell cat ${INVENTORY_FILE} | grep bastion | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}' | sort -u | head -n 1))
	$(eval bastion_key := $(shell cat ${INVENTORY_FILE} | grep bastion | grep  ansible_ssh_private_key_file | sed -e "s/.*ansible_ssh_private_key_file=//"))
	$(eval bastion_user := $(shell cat ${INVENTORY_FILE} | grep bastion | grep  ansible_user | sed -e "s/.*ansible_user=//" -e "s/ .*//"))
	@if [ -z ${reporter} ]; then REPORTER_CLI=""; else REPORTER_CLI="--reporter ${reporter}"; fi && \
 	$(DOCKER) run -t --rm -v ${PWD}:/share ${IMAGE_INSPEC} exec tests/inspec/devsec-bastion $${REPORTER_CLI} --chef-license accept-no-persist --no-distinct-exit --sudo --no-create-lockfile -t ssh://${bastion_user}@${bastion_ip} -i ${bastion_key}

inspec-dev-sec: ## dev-sec.io tests with Inspec for host. Argument: host=master-1|worker-1|... [reporter=progress|json|...]
inspec-dev-sec:
	test -n "${host}"  # Failed if host not set
	$(eval bastion_ip := $(shell cat ${INVENTORY_FILE} | grep bastion | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}' | sort -u | head -n 1))
	$(eval bastion_key := $(shell cat ${INVENTORY_FILE} | grep bastion | grep  ansible_ssh_private_key_file | sed -e "s/.*ansible_ssh_private_key_file=//"))
	$(eval server_user := $(shell cat ${INVENTORY_FILE} | grep bastion | grep  ansible_user | sort -u | head -n 1 | sed -e "s/.*ansible_user=//" -e "s/ .*//"))
	$(eval host_ip := $(shell cat ${INVENTORY_FILE} | grep ${host} | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}' | sort -u | head -n 1))
	$(eval host_key := $(shell cat ${INVENTORY_FILE} | grep ${host} | grep  ansible_ssh_private_key_file | sort -u | head -n 1 | sed -e "s/.*ansible_ssh_private_key_file=//"))
	@if [ -z ${reporter} ]; then REPORTER_CLI=""; else REPORTER_CLI="--reporter ${reporter}"; fi && \
	$(DOCKER) run -t --rm -v ${PWD}:/share ${IMAGE_INSPEC} exec tests/inspec/devsec $${REPORTER_CLI} --input ip_bastion=${bastion_ip} --chef-license accept-no-persist --no-distinct-exit --sudo --no-create-lockfile --proxy-command="ssh ${server_user}@${bastion_ip} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${bastion_key} -W %h:%p" -t ssh://${server_user}@${host_ip} -i ${host_key}

get-inspec-dev-sec-profile: ## Get Inspec dev-sec.io profiles.
get-inspec-dev-sec-profile:
	@mkdir -p .inspec
	@rm -rf .inspec/linux-baseline
	${GIT} clone --single-branch --branch master https://github.com/dev-sec/linux-baseline.git .inspec/linux-baseline
	@rm -rf .inspec/linux-baseline/.git
	@rm -rf .inspec/ssh-baseline
	${GIT} clone --single-branch --branch master https://github.com/dev-sec/ssh-baseline.git .inspec/ssh-baseline
	@rm -rf .inspec/ssh-baseline/.git

.PHONY: ansible-lint inspec-dev-sec-bastion inspec-dev-sec get-inspec-dev-sec-profile

## Admin
## -----
ssh: ## SSH connexion. Argument: host=bastion|worker-1|...
ssh:
	test -n "${host}"  # Failed if host not set
	$(eval host_ip := $(shell cat ${INVENTORY_FILE} | grep ${host} | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}' | sort -u | head -n 1))
	$(eval host_key := $(shell cat ${INVENTORY_FILE} | grep ${host} | grep  ansible_ssh_private_key_file | sort -u | head -n 1 | sed -e "s/.*ansible_ssh_private_key_file=//"))
	@if [ "${host}" = "bastion" ]; then \
		$(SSH) -F .ssh/ssh_config ${host}; \
	else \
		$(SSH) -F .ssh/ssh_config -i ${host_key} ${host_ip}; \
	fi

ssh-init: ## SSH direct initial connexion. Argument: ip=w.x.y.z
ssh-init:
	test -n "${ip}"  # Failed if ip not set
	$(SSH) -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@${ip}

change-key: ## Change SSH key. Argument: group=bastions old_key=k-bastion-1 new_key=k-bastion-2
change-key:
	test -n "${group}"  # Failed if group not set
	test -n "${old_key}"  # Failed if old_key not set
	test -n "${new_key}"  # Failed if new_key not set
	@if [ -z ${container} ]; then \
		$(DOCKER) run -t --rm -v ${PWD}:/ansible -e ANSIBLE_CONFIG=.ansible/ansible.cfg ${IMAGE_ANSIBLE} \
		ansible-playbook --limit ${group} -i ${INVENTORY_FILE} -e "key_name=${new_key} key_state=present" provisioning/key.yml; \
	else \
		export ANSIBLE_CONFIG=.ansible/ansible.cfg && ansible-playbook --limit ${group} -i ${INVENTORY_FILE} -e "key_name=${new_key} key_state=present" provisioning/key.yml; \
	fi
	@if [ -z ${container} ]; then \
		$(DOCKER) run -t --rm -v ${PWD}:/ansible -e ANSIBLE_CONFIG=.ansible/ansible.cfg ${IMAGE_ANSIBLE} \
		ansible-playbook --limit ${group} -i ${INVENTORY_FILE} --key-file "../.ssh/${new_key}.pem" -e "key_name=${old_key} key_state=absent" provisioning/key.yml; \
	else \
		export ANSIBLE_CONFIG=.ansible/ansible.cfg && ansible-playbook --limit ${group} -i ${INVENTORY_FILE} --key-file "../.ssh/${new_key}.pem" -e "key_name=${old_key} key_state=absent" provisioning/key.yml; \
	fi
	@sed -i.bu 's%${old_key}%${new_key}%g' ${INVENTORY_FILE}
	@rm -f ${INVENTORY_FILE}.bu
	@$(MAKE) set-ssh-config
	@echo "inventory and ssh_config files updated!"
	@echo " "

set-env: ## Set environment. Prerequisites: .vault_password file
set-env:
	@mkdir -p .ansible .ssh
	@cp files/ansible.cfg .ansible/
	@chmod 700 .ansible
	@$(MAKE) ansible-vault-decrypt source=files/inventory.vault dest=.ansible/inventory
	$(eval keys := $(shell find ${KEY_PATH} -maxdepth 1 -type f -name "*.vault" | sed -e "s/files\/keys\///g" -e "s/\.vault//g"))
	@echo ${keys}
	@for key in ${keys}; do \
		$(MAKE) ansible-vault-decrypt source=${KEY_PATH}/$${key}.vault dest=.ssh/$${key}; \
	done

set-ssh-config: ## Set SSH config file.
set-ssh-config:
	@mkdir -p .ssh
	@cp -f files/ssh_config .ssh/ssh_config
	$(eval bastion_ip := $(shell cat ${INVENTORY_FILE} | grep bastion | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}' | sort -u | head -n 1))
	$(eval bastion_key := $(shell cat ${INVENTORY_FILE} | grep bastion | grep  ansible_ssh_private_key_file | sed -e "s/.*ansible_ssh_private_key_file=//"))
	$(eval server_user := $(shell cat ${INVENTORY_FILE} | grep bastion | grep  ansible_user | sed -e "s/.*ansible_user=//" -e "s/ .*//"))
	$(eval network_prefix := $(shell cat ${INVENTORY_FILE} | grep bastion | egrep -o '([0-9]{1,3}\.){2}[0-9]{1,3}' | sort -u | head -n 1))
	@sed -i.bu 's/__BASTION_IP__/${bastion_ip}/g' .ssh/ssh_config
	@sed -i.bu 's%__BASTION_KEY_PATH__%${bastion_key}%g' .ssh/ssh_config
	@sed -i.bu 's/__SERVER_USER__/${server_user}/g' .ssh/ssh_config
	@sed -i.bu 's/__NETWORK_PREFIX__/${network_prefix}/g' .ssh/ssh_config
	@rm -f .ssh/ssh_config.bu
	@chmod 600 .ssh/ssh_config
	@echo ".ssh/ssh_config file updated"

update: ## Update dev-sec profile, docker images
update:
	@$(MAKE) ansible-galaxy-install
	@$(MAKE) get-inspec-dev-sec-profile
	$(DOCKER) pull ${IMAGE_ANSIBLE}
	$(DOCKER) pull ${IMAGE_INSPEC}

generate-key: ## generate RSA key. Argument: key=k-bastion-1
generate-key:
	test -n "${key}"  # Failed if key not set
	@mkdir -p .ssh
	ssh-keygen -q -N "" -m PEM -t rsa -b 4096 -f .ssh/${key}.pem -C "${key_name}"
	@mv .ssh/${key}.pem.pub .ssh/${key}.pub
	@cp .ssh/${key}.pub .ssh/${key}.pub.v
	@cp .ssh/${key}.pem .ssh/${key}.pem.v
	@$(MAKE) ansible-vault-encrypt file=.ssh/${key}.pub.v
	@$(MAKE) ansible-vault-encrypt file=.ssh/${key}.pem.v
	@mv .ssh/${key}.pub.v files/keys/${key}.pub.vault
	@mv .ssh/${key}.pem.v files/keys/${key}.pem.vault
	@echo "Key generated and encrypted"

scan: ## Scan nmap
scan:
	$(eval network_prefix := $(shell cat ${INVENTORY_FILE} | grep bastion | egrep -o '([0-9]{1,3}\.){2}[0-9]{1,3}' | sort -u | head -n 1))
	@nmap -sP ${network_prefix}.0-150

.PHONY: ssh change-key set-env set-ssh-config update generate-key scan

## Kubectl
## -------
get-nodes: ## Get nodes
get-nodes:
	@export KUBECONFIG=$KUBECONFIG:.kube/config && $(KUBECTL) get nodes -o wide

get-pods: ## Get pods
get-pods:
	@export KUBECONFIG=$KUBECONFIG:.kube/config && $(KUBECTL) get pods -o wide --all-namespaces

get-svc: ## Get services
get-svc:
	@export KUBECONFIG=$KUBECONFIG:.kube/config && $(KUBECTL) get services --all-namespaces --sort-by=.metadata.name

top: ## top on nodes
top:
	@export KUBECONFIG=$KUBECONFIG:.kube/config && $(KUBECTL) top nodes

proxy-dashboard: ## Run kubectl proxy
proxy-dashboard:
	@export KUBECONFIG=$KUBECONFIG:.kube/config && $(KUBECTL) -n kubernetes-dashboard describe secret admin-user-token | grep ^token
	@echo "---"
	@echo "Browse: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/"
	@echo "---"
	@export KUBECONFIG=$KUBECONFIG:.kube/config && $(KUBECTL) proxy

get-kubeconfig: ## Get kubeconfig file from master. After run: export KUBECONFIG=$KUBECONFIG:.kube/config
get-kubeconfig:
	@mkdir -p .kube
	@$(DOCKER) run -t --rm -v ${PWD}:/ansible -e ANSIBLE_CONFIG=.ansible/ansible.cfg ${IMAGE_ANSIBLE} \
		ansible-playbook --limit master-1 -i ${INVENTORY_FILE} provisioning/get-kubeconfig.yml
	$(eval host_ip := $(shell cat ${INVENTORY_FILE} | grep master-1 | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}' | sort -u | head -n 1))
	@$(KUBECTL) config set-cluster default --server=https://${host_ip}:6443 --kubeconfig .kube/config
	@echo "run command: export KUBECONFIG=$KUBECONFIG:.kube/config"

.PHONY: get-nodes get-pods get-svc top get-dashboard-token get-kubeconfig

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
