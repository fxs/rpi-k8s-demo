# encoding: utf-8
# copyright: 2020, The Authors

title 'Devsec checks'

include_controls 'devsec-linux' do
    # IPv4 Forwarding (Docker)
    skip_control 'sysctl-01'
    # IPv6 Forwarding (Docker)
    skip_control 'sysctl-19'
    # CPU No execution Flag or Kernel ExecShield (For AMD64)
    skip_control 'sysctl-33'
end

include_controls 'devsec-ssh' do
    # ssh ciphers, Algorithm, Codes
    skip_control 'sshd-01'
    skip_control 'sshd-02'
    skip_control 'sshd-03'
    skip_control 'ssh-08'
    skip_control 'ssh-09'
    skip_control 'ssh-10'
    # UsePrivilegeSeparation deprecated
    skip_control 'sshd-16'
end
