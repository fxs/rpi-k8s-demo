#!/bin/sh

set -e
docker version >/dev/null 2>/dev/null
docker system prune -f -a --volumes
