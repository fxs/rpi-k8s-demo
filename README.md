# RPi K8s Demo

[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)

Deployment of a kubernetes cluster on Raspberry pi 3 and 4:

* OS [Ubuntu 64bits](https://ubuntu.com/download/raspberry-pi)
* Kubernetes distribution [k3s](https://k3s.io/)
* Provisioning [Ansible](https://www.ansible.com/)
* Security rules [DevSec Hardening Framework](https://dev-sec.io/)

![Raspberry Pi 3 and 4 cluster](rpi.jpg)

## Getting Started

### Requirements

In order to deploy this environment:

* [docker](https://www.docker.com/)
* [make](https://www.gnu.org/software/make/)

Prerequisite files:

* vault_password file
* vault encrypted keys in files/keys directory
* encrypted inventory file in files directory

### Installation

#### Set local environment to deploy servers

Set inventory file.

```shell
make set-env
make update
make set-ssh-config
# Generate keys
make generate key=k-bastion-1
make generate key=k-master-1
make generate key=k-worker-1
```

#### Installation of Raspberry Pi

Flash SD card with Ubuntu 64 bits.
Add an empty ssh file at the root file system of the SD card to enable ssh server.

```shell
#Boot and change ubuntu password by connecting for the first time
make ssh-init ip=p_dhcp
# Set static IP and ssh connexion key
make host-init host=bastion host_ip=ip_dhcp key_name=k-bastion-1 password=ubuntu_password
# Run provisioning for the host
make provision group=bastions args=provisioning/core.yml
```

List of available commandes:

```shell
make
```

## Tests

To lint Ansible playbooks:

```shell
make ansible-lint
```

To run security [Chef Inspec](https://www.chef.io/products/chef-inspec/) tests with [DevSec Hardening Framework](https://dev-sec.io/) OS and SSH Baselines:

```shell
make inspec-dev-sec-bastion
make inspec-dev-sec host=master-1
make inspec-dev-sec host=node-1
...
```

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See the [LICENSE](https://gitlab.com/fxs/vagrant-virtualbox-ansible-k3s/blob/master/LICENSE) file for details.
